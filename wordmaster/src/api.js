const api =async (method)=>{

if (method==='GET'){

  const url = 'https://words.dev-apis.com/word-of-the-day';
  const response = await fetch(url);
  const result = await response.json();
  const gameWord = result.word
  const word1 = gameWord.toUpperCase();

  return word1
  }

 else {
  const posturl = 'https://words.dev-apis.com/validate-word';
  const apiKey = '088c4666bfb7f8cf960ef7dfb162de39b14136893a8a4f4bec57ffadd7e44fab';
  const wordToValidate =method;
  let isValid =''
  
  const response=await fetch(posturl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${apiKey}`
    },
    body: JSON.stringify({word: wordToValidate})
  })
  
const data =await response.json()
return data
}
}

export default api