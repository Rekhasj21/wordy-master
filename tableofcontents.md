# TABLE OF CONTENTS

## Welcome to the Class

1. Introduction
2. Get Set Up
3. What Are You Going to Learn

## HTML

1. Tags
2. Types of Tags
3. Attributes
4. Organizing HTML
5. Head and Meta Tags
6. HTML Project

## CSS

1. Rules
2. Selectors and the Cascade
3. Pseudoclasses and Pseudoelements
4. Layout CSS
5. Flex
6. Grid
7. Animations
8. Putting It Together
9. Project

## JavaScript

1. Intro
2. Numbers, Strings, and Booleans
3. Control Flow
4. Loops
5. Exercise
6. Functions
7. Scope
8. Builtins
9. Objects
10. Context
11. Arrays

## Putting It All Together

1. The DOM
2. Events and Listeners
3. Project
4. Talking to Servers
5. JSON
6. AJAX
7. async/await
8. Project

## Other Stuff You Should Know

1. Using Third Party Libraries
2. Git and GitHub
3. Things to Do Next
4. Conclusion.
