# Introduction

## HTML

HTML stands for hypertext markup language. You can think of it as the content on a webpage. As you're reading this content right now, the images and text are written in HTML. It's also the structure of the content. You can group bits of HTML together.

## CSS

CSS stands for cascading stylesheets.
Style of a website like font,background color,spacing,layout etc.
CSS is basically just a series of rules that says "if an element matches this selector, apply this style."

## Javascript

JavaScript is the programming language we are going to use today. There are many programming languages like C++, Python, Go, PHP, and many, many more. We are choosing JavaScript for the exact reason that every browser (Chrome, Firefox, etc.) can run JavaScript on a web page (whereas it cannot run any other language.) JavaScript was specifically invented for the purpose of running on webpages but it has now grown beyond that and is being run in a great many places. The skills you will learn writing JavaScript will translate to other languages.

### For Example, In building a car

**HTML** is materials of car used like the frame, the doors, the steering wheel, the tires, etc. They need to be arranged into a coherent car like color of the car,the styling,via **CSS**. It needs an engine with all the wiring via **JavaScript** to actually do anything.

HTML and CSS describe a non-interactive webpage, like a page in a book. All the content, pictures, fonts, spacing, etc. is all there and it can be read. The page in a book is not interactive though; if you try to touch your book like a touchscreen, well, it's not going to do much. Think of JavaScript being the piece that transform a non-interactive page out of a book to a touchscreen app. Whereas you had text, font, colors, images, etc. all before, now you can have things move around, have pop ups, refresh content, start animations, all sorts of stuff.
