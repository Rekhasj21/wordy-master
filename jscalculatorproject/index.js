


let screen = document.querySelector('.screen');
let buttons = document.querySelectorAll('.btn');
let clear = document.querySelector('.btn-clear');
let equal = document.querySelector('.btn-equal');
let length = document.querySelector('.btn-length');

length.addEventListener('click',function(event){
    let answer = screen.value;
    let index = answer.length
    console.log((index))
    answer = answer.slice(0,index-1)
    screen.value = answer;
})  

buttons.forEach(function(button){
    button.addEventListener('click', function(event){
        let value = event.target.dataset.num;
        screen.value += value;
      })
});
    
equal.addEventListener('click', function(event){
    if(screen.value === ''){
        screen.value = '';
      } else {
        let answer = eval(screen.value);
        screen.value = answer;
      }
})
    
clear.addEventListener('click', function(event){
      screen.value = '';
})
   
  